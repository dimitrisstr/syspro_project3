#include "communication.h"

int sendToClient(int socket, char *message)
{
	int sent = 0, n, size;
	uint32_t sizeN;
	size = strlen(message) + 1;
	sizeN = htonl(size);	//convert to network byte order
	if(write(socket, &sizeN, sizeof(uint32_t)) < 0)	//send the size of the message
		return -1;

	while(sent < size)
	{
		if((n = write(socket, message + sent, size - sent)) == -1) //send the message
			return -1;
		sent += n;
	}
	return 0;
}


int readFromClient(int socket, char **command)
{
	int nread, total = 0;
	uint32_t size;
	if(read(socket, &size, sizeof(size)) <= 0)	//number of bytes that will be read
		return -1;
	size = ntohl(size);
	if((*command = malloc(sizeof(char) * size)) == NULL)
		return ALLOCATE_ERROR;

	while(total < size)
	{
		if((nread = read(socket, *command + total, size - total)) <= 0)
			return -1;
		total += nread;
	}
	return 0;
}
