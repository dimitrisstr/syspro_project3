#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "hash.h"
#include "list.h"

#define ALLOCATE_ERROR -3

int sendToClient(int socket, char *message);

int readFromClient(int socket, char **command);

int addAccount(int socket, Hash hash, char *name, int amount, int delay);

int addTransfer(int socket, Hash hash, char *source, char *dest, int amount, int delay);

int checkAccounts(Hash hash, list accounts_list);

int addMultiTransfer(int socket, Hash hash, list accounts_list, int amount, int delay);

int printBalance(int socket, Hash hash, char *name);

int printMultiBalance(int socket, Hash hash, list accounts_list);



#endif /* FUNCTIONS_H_ */
