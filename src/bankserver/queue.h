#ifndef QUEUE_H_
#define QUEUE_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct queue* Queue;

int Queue_create(Queue *queue, int size);

int Queue_add(Queue queue, int element);

int Queue_remove(Queue queue);

int Queue_isEmpty(Queue queue);

int Queue_isFull(Queue queue);

void Queue_destroy(Queue *queue);

#endif /* QUEUE_H_ */
