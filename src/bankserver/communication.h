#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#define ALLOCATE_ERROR -3

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

int sendToClient(int socket, char *message);

int readFromClient(int socket, char **command);

#endif /* COMMUNICATION_H_ */
