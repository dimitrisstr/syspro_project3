#include "account.h"

typedef struct account
{
	char *name;
	int balance;
	list inAmountList;
} Account;

typedef struct transfer
{
	char *name;
	int amount;
} transfer;

int Account_create(Accountptr *account, char *name, int balance)
{
	//create a new account
	if((*account = malloc(sizeof(struct account))) == NULL)
		return -1;
	if(((*account)->name = malloc(sizeof(char) * (strlen(name) + 1))) == NULL)
	{
		free(*account);
		return -1;
	}
	strcpy((*account)->name, name);
	(*account)->balance = balance;
	List_create(&(*account)->inAmountList);
	return 0;
}

int Account_returnBalance(Accountptr account)
{
	return account->balance;
}

int Account_addTransfer(Accountptr sourceAccount, Accountptr destAccount, int amount)
{
	transferptr destTransfer;

	//checks if a transfer with the same name already exists
	destTransfer = List_find(destAccount->inAmountList, sourceAccount, Account_compare);
	if(destTransfer != NULL)
	{
		destTransfer->amount += amount;
		destAccount->balance += amount;
		sourceAccount->balance -= amount;
	}
	else
	{
		//create a new transfer
		char *sourceName = sourceAccount->name;
		if((destTransfer = malloc(sizeof(transfer))) == NULL)
			return -1;
		if((destTransfer->name = malloc(sizeof(char) * (strlen(sourceName) + 1))) == NULL)
		{
			free(destTransfer);
			return -1;
		}
		strcpy(destTransfer->name, sourceName);
		destTransfer->amount = amount;
		//update the balance of the accounts
		sourceAccount->balance -= amount;
		destAccount->balance += amount;
		List_add(destAccount->inAmountList, destTransfer, Account_compare);
	}
	return 0;
}

int Account_compare(void *account1, void *account2)
{
	Accountptr acc1 = account1, acc2 = account2;
	return strcmp(acc1->name, acc2->name);
}

void Account_delete(Accountptr account)
{
	transferptr transf;
	iterator it;
	List_initIterator(account->inAmountList, &it);
	while(it != NULL)
	{
		transf = List_returnValue(&it);
		free(transf->name);
	}
	free(account->name);
	free(account);
}


