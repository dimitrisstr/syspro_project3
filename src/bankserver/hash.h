#ifndef HASH_H_
#define HASH_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#include "account.h"
#include "list.h"

typedef struct hash* Hash;

int Hash_create(Hash *hash, int size);

int Hash_addAccount(Hash hash, char *name, int balance);

int Hash_addTransfer(Hash hash, int amount, char *source, char *dest);

void Hash_lock(Hash hash, int index);

void Hash_unlock(Hash hash, int index);

Accountptr Hash_getAccount(Hash hash, char *name);

int Hash_returnBalance(Hash hash, char *name);

int Hash_hashFunction(Hash hash, char *name);

void Hash_destroy(Hash *hash);

#endif /* HASH_H_ */
