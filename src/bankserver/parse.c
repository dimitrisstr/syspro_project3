#include "parse.h"


void readCommands(int socket, Hash accounts_hash)
{
	int error = 0;
	char *command;
	while(error != EXIT)
	{
		if(readFromClient(socket, &command) < 0)	//client closed socket
			break;
		printf("thread: %ld   command: %s\n", pthread_self(), command);
		error = executeCommand(command, socket, accounts_hash);
		free(command);
		if(error == PARSE_ERROR)
			error = sendToClient(socket, "Error. Unknown command\n");
		else if(error == ALLOCATE_ERROR)
			error = sendToClient(socket, "Error. Command failed\n");
	}
	printf("Client exit\n");
	close(socket);
}

int executeCommand(char *command, int socket, Hash accounts_hash)
{
	/*
	 * returns  0 in case of success
	 * returns -2 in case of parse error
	 * returns -3 in case of allocation error
	 */

	char *saveptr, *token = NULL;
	int error = 0;
	if((token = strtok_r(command, " \n", &saveptr)) == NULL)
		return PARSE_ERROR;

	if(strcmp(token, "add_account") == 0)
	{
		/*
		 * add account <init amount> <name> [delay]
		 */
		char *name;
		int init_amount, delay = 0;
		//	<init_amount>
		if(((token = strtok_r(NULL, " \n", &saveptr)) == NULL) || isNumber(token) < 0)
			return PARSE_ERROR;
		init_amount = atoi(token);

		// <name>
		if((token = strtok_r(NULL, " \n", &saveptr)) == NULL)
			return PARSE_ERROR;
		if((name = malloc(sizeof(char) * (strlen(token) + 1))) == NULL)
			return ALLOCATE_ERROR;
		strcpy(name, token);

		// [delay]
		if(((token = strtok_r(NULL, " \n", &saveptr)) != NULL) && isNumber(token) == 1)
			delay = atoi(token);

		error = addAccount(socket, accounts_hash, name, init_amount, delay);
		free(name);
	}
	else if(strcmp(token, "add_transfer") == 0)
	{
		/*
		 *  add transfer <amount> <src name> <dst name> [delay]
		 */
		char *src_name, *dst_name;
		int amount, delay = 0;
		// <amount>
		if(((token = strtok_r(NULL, " \n", &saveptr)) == NULL) || isNumber(token) < 0)
			return PARSE_ERROR;
		amount = atoi(token);

		// <src_name>
		if((token = strtok_r(NULL, " \n", &saveptr)) == NULL)
			return PARSE_ERROR;
		if((src_name = malloc(sizeof(char) * (strlen(token) + 1))) == NULL)
			return ALLOCATE_ERROR;
		strcpy(src_name, token);

		// <dst_name>
		if((token = strtok_r(NULL, " \n", &saveptr)) == NULL)
			return PARSE_ERROR;
		if((dst_name = malloc(sizeof(char) * (strlen(token) + 1))) == NULL)
		{
			free(src_name);
			return ALLOCATE_ERROR;
		}
		strcpy(dst_name, token);

		// [delay]
		if(((token = strtok_r(NULL, " \n", &saveptr)) != NULL) && isNumber(token) == 1)
			delay = atoi(token);

		error = addTransfer(socket, accounts_hash, src_name, dst_name, amount, delay);
		free(src_name);
		free(dst_name);
	}
	else if(strcmp(token, "add_multi_transfer") == 0)
	{
		/*
		 * add multi transfer <amount> <src name> <dst name1> <dst name2> ...[delay]
		 */
		int amount;
		if(((token = strtok_r(NULL, " \n", &saveptr)) == NULL) || (isNumber(token) < 0))
			return PARSE_ERROR;
		amount = atoi(token);
		char *name;
		list accounts_list;
		List_create(&accounts_list);
		while((token = strtok_r(NULL, " \n", &saveptr)) != NULL)
		{
			if((name = malloc(sizeof(char) * (strlen(token) + 1))) == NULL)
			{
				List_destroy(&accounts_list);
				return ALLOCATE_ERROR;
			}
			strcpy(name, token);
			List_addLast(accounts_list, name);
		}

		int delay = 0;
		if(isNumber(name) == 1)
		{
			delay = atoi(name);
			token = List_removeLast(accounts_list);
			free(token);
		}
		if(List_returnSize(accounts_list) < 2)
		{
			List_destroy(&accounts_list);
			return PARSE_ERROR;
		}

		error = addMultiTransfer(socket, accounts_hash, accounts_list, amount, delay);
		List_destroy(&accounts_list);
	}
	else if(strcmp(token, "print_balance") == 0)
	{
		/*
		 *  print balance <name>
		 */
		char *name;
		if((token = strtok_r(NULL, " \n", &saveptr)) == NULL)
			return PARSE_ERROR;
		if((name = malloc(sizeof(char) * (strlen(token) + 1))) == NULL)
			return ALLOCATE_ERROR;
		strcpy(name, token);
		error = printBalance(socket, accounts_hash, name);
		free(name);
	}
	else if(strcmp(token, "print_multi_balance") == 0)
	{
		/*
		 *  print multi balance <name1> <name2> ...
		 */
		char *name;
		list accounts_list;
		List_create(&accounts_list);
		while((token = strtok_r(NULL, " \n", &saveptr)) != NULL)
		{
			if((name = malloc(sizeof(char) * (strlen(token) + 1))) == NULL)
			{
				List_destroy(&accounts_list);
				return ALLOCATE_ERROR;
			}
			strcpy(name, token);
			List_addLast(accounts_list, name);
		}
		if(List_isEmpty(accounts_list))
		{
			List_destroy(&accounts_list);
			return PARSE_ERROR;
		}
		error = printMultiBalance(socket, accounts_hash, accounts_list);
		List_destroy(&accounts_list);
	}
	else
		return PARSE_ERROR;

	return error;
}

int isNumber(char *token)
{
	int i;
	for(i = 0; i < strlen(token); i++)
	{
		if(token[i] < '0' || token[i] > '9')
			return -1;
	}
	return 1;
}
