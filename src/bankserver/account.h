#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

typedef struct account* Accountptr;
typedef struct account Account;
typedef struct transfer* transferptr;

int Account_create(Accountptr *account, char *name, int balance);

int Account_returnBalance(Accountptr account);

int Account_addTransfer(Accountptr sourceAccount, Accountptr destAccount, int amount);

int Account_compare(void *account1, void *account2);

void Account_delete(Accountptr account);

#endif /* ACCOUNT_H_ */
