#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>

#define ALLOCATERROR -1
#define VALUE_EXISTS -2

typedef struct infoNode* list;
typedef struct listNode* iterator;

int List_create(list *lst);

int List_add(list lst, void *data, int (*Compare)(void *, void *));

int List_addFirst(list lst, void *data);

int List_addLast(list lst, void *data);

void *List_find(list lst, void *cvalue, int (*Compare)(void *, void *));

void *List_removeLast(list lst);

int List_destroy(list *lst);

int List_isEmpty(list lst);

int List_returnSize(list lst);

void* List_returnValue(iterator *it);

void List_initIterator(list lst, iterator *it);

#endif /* LIST_H */
