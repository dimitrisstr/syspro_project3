#include "hash.h"

typedef struct hashElement
{
	pthread_mutex_t mutex;
	list accountsList;
} hashElement;

typedef struct hash
{
	int size;
	hashElement *hashTable;

} hash;

int Hash_create(Hash *hash, int size)
{
	if((*hash = malloc(sizeof(struct hash))) == NULL)
		return -1;
	(*hash)->size = size;
	if(((*hash)->hashTable = malloc(sizeof(hashElement) * size)) == NULL)
		return -1;

	int i;
	for(i = 0; i < size; i++)
	{
		if(List_create(&(*hash)->hashTable[i].accountsList) < 0)
			return -1;
		pthread_mutex_init(&((*hash)->hashTable[i].mutex), NULL);
	}
	return 0;
}

void Hash_lock(Hash hash, int index)
{
	pthread_mutex_lock(&hash->hashTable[index].mutex);
}

void Hash_unlock(Hash hash, int index)
{
	pthread_mutex_unlock(&hash->hashTable[index].mutex);
}

int Hash_addAccount(Hash hash, char *name, int balance)
{
	int error;
	Accountptr newAccount;
	if(Account_create(&newAccount, name, balance) < 0)
		return -1;
	int index = Hash_hashFunction(hash, name);
	error = List_add(hash->hashTable[index].accountsList, newAccount, &Account_compare);
	return error;
}

int Hash_addTransfer(Hash hash, int amount, char *source, char *dest)
{
	Accountptr src_account, dst_account;
	if((src_account = Hash_getAccount(hash, source)) == NULL || (dst_account = Hash_getAccount(hash, dest)) == NULL)
		return -1;
	else
	{
		int sourceBalance;
		sourceBalance = Account_returnBalance(src_account);
		if(amount <= sourceBalance)
		{
			if(Account_addTransfer(src_account, dst_account, amount) < 0)
				return -1;
		}
		else
			return -1;
	}
	return 0;
}

Accountptr Hash_getAccount(Hash hash, char *name)
{
	int index = Hash_hashFunction(hash, name);
	Accountptr acc, tempAcc;
	Account_create(&tempAcc, name, 0);	//create a temporary account with the same name
	list lst = hash->hashTable[index].accountsList;
	acc = List_find(lst, tempAcc, Account_compare);
	return acc;
}

int Hash_returnBalance(Hash hash, char *name)
{
	Accountptr acc = Hash_getAccount(hash, name);
	if(acc == NULL)
		return -1;
	else
		return Account_returnBalance(acc);
}

int Hash_hashFunction(Hash hash, char *name)
{
	int i,index = 0;
	for(i = 0; i < strlen(name); i++)
		index += (name[i] << i) + i;
	return index % hash->size;
}

void Hash_destroy(Hash *hash)
{
	int i;
	for(i = 0; i < (*hash)->size; i++)
	{
		pthread_mutex_destroy(&((*hash)->hashTable[i].mutex));
		iterator it;
		List_initIterator((*hash)->hashTable[i].accountsList, &it);
		while(it != NULL)
			Account_delete(List_returnValue(&it));
	}
	free(*hash);
}
