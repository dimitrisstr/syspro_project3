
#include "list.h"

//data node
typedef struct listNode
{
    void *data;
    struct listNode* next;
} listNode;

//information node
typedef struct infoNode
{
    int size;
    listNode* start;
    listNode* last;
} infoNode;



int List_create(list *lst)
{
    //allocate a new infoNode
    *lst = malloc(sizeof(infoNode));
    if(*lst == NULL)
        return ALLOCATERROR;
    //initialize variables
    (*lst)->size = 0;
    (*lst)->start = NULL;
    (*lst)->last = NULL;
    return 1;
}

int List_add(list lst, void *data, int (*Compare)(void *, void *))
{
	//add an element at the ordered list
	if(lst->size == 0)	//if list is empty add first
	{
		List_addFirst(lst, data);
		return 0;
	}
	else if((*Compare)(lst->start->data, data) > 0)	//if first element is bigger
	{												//add first
		List_addFirst(lst, data);
		return 0;
	}
	else
	{
		//create a new node
		listNode *newNode = malloc(sizeof(listNode));
		if(newNode == NULL)
			return ALLOCATERROR;
		newNode->data = data;
		listNode *nextNode, *previousNode;
		previousNode = lst->start;
		nextNode = previousNode;

		while(nextNode != NULL)
		{
			if((*Compare)(nextNode->data, data) == 0)	//return error value exists
				return VALUE_EXISTS;
			else if((*Compare)(nextNode->data, data) > 0)	//found position
				break;
			previousNode = nextNode;
			nextNode = nextNode->next;
		}
		//add the new node
		previousNode->next = newNode;
		//if newNode is last nextNode is NUll
		newNode->next = nextNode;
		lst->size++;
		return 0;
	}
}

void *List_find(list lst, void *cvalue, int (*Compare)(void *, void *))
{
	void *value;
	iterator it;
	List_initIterator(lst, &it);
	while(it != NULL)
	{
		value = List_returnValue(&it);
		if((*Compare)(value, cvalue) == 0)
			return value;
		else if((*Compare)(value, cvalue) > 0)
			return NULL;
	}
	return NULL;
}

int List_addFirst(list lst, void *data)
{
    //add a node at the start of the list
    listNode *temp = lst->start;
    if( (lst->start = malloc(sizeof(listNode))) == NULL)
        return ALLOCATERROR;

    lst->start->next = temp;
    lst->start->data = data;
    lst->size++;

    return 1;
}

int List_addLast(list lst, void *data)
{
    //add a node a the end of the list
    listNode *newNode = malloc(sizeof(listNode));
    if(newNode == NULL)
        return ALLOCATERROR;
    newNode->data = data;
    newNode->next = NULL;
    if(lst->last == NULL)
    {
        lst->start = newNode;
        lst->last = newNode;
    }
    else
    {
        lst->last->next = newNode;
        lst->last = newNode;
    }
    lst->size++;
    return 1;
}

void *List_removeLast(list lst)
{
	void *returnValue;
	if(lst->start == NULL)
		return NULL;
	else if(lst->start == lst->last)
	{
		returnValue = lst->start->data;
		free(lst->last);
		lst->start = NULL;
		lst->last = NULL;
		lst->size--;
	}
	else
	{
		int i;
		listNode *temp = lst->start;
		for(i = 0; i < lst->size - 2; i++)
			temp = temp->next;
		returnValue = lst->last->data;
		free(lst->last);
		lst->last = temp;
		temp->next = NULL;
		lst->size--;
	}
	return returnValue;
}

int List_destroy(list *lst)
{
	//destroy list
	listNode *temp1 = (*lst)->start;
	listNode *temp2;
	while((*lst)->size != 0)
	{
		temp2 = temp1;
		temp1 = temp1->next;
		free(temp2->data);
		free(temp2);
		(*lst)->size--;
	}
	free(*lst);
	return 0;
}

void List_initIterator(list lst, iterator *it)
{
    //initialize the iterator to point at the start of the list
    *it = lst->start;
}

int List_isEmpty(list lst)
{
	return lst->size == 0;
}

int List_returnSize(list lst)
{
    //return the size of the list
    return lst->size;
}

void* List_returnValue(iterator *it)
{
    /*
     * return the data of the node that the iterator
     * points to and move the iterator to the next node
     */
    void *data;
    while(*it != NULL)
    {
        data = (*it)->data;
        *it = (*it)->next;
        return data;
    }
    return NULL;
}
