#include "functions.h"


int max(int i, int j);
int min(int i, int j);
int int_compare(void *integer1, void *integer2);


int checkAccounts(Hash hash, list accounts_list)
{
	//check if all accounts in the accounts_list exist
	iterator it;
	List_initIterator(accounts_list, &it);
	while(it != NULL)
	{
		char *name = List_returnValue(&it);
		if(Hash_getAccount(hash, name) == NULL)	//account not found
			return -1;
	}
	return 0;
}

int addAccount(int socket, Hash hash, char *name, int amount, int delay)
{
	char *response_message;
	char *success_message = "Success. Account creation (";
	char *error_message = "Error. Account creation failed(";
	response_message = malloc(sizeof(char) * (strlen(error_message) + strlen(name) + sizeof(int) + 15));
	if(response_message == NULL)
		return ALLOCATE_ERROR;

	int index = Hash_hashFunction(hash, name);
	Hash_lock(hash, index);	//lock mutex
	if(Hash_addAccount(hash, name, amount) < 0)	//error
		sprintf(response_message, "%s%s:%d)\n", error_message, name, amount);
	else
		sprintf(response_message, "%s%s:%d)\n", success_message, name, amount);

	usleep(delay * 1000);	//sleep for 'delay' milliseconds
	Hash_unlock(hash, index);	//unlock mutex

	sendToClient(socket, response_message);
	free(response_message);
	return 0;
}


int addTransfer(int socket, Hash hash, char *source, char *dest, int amount, int delay)
{
	char *success_message = "Success. Transfer addition (";
	char *error_message = "Error. Transfer addition failed (";
	char *response_message = malloc(sizeof(char) * (strlen(error_message) + strlen(source) +
															strlen(dest) + 8 + 2 * sizeof(int)));
	if(response_message == NULL)
		return ALLOCATE_ERROR;
	int sourceIndex = Hash_hashFunction(hash, source), destIndex = Hash_hashFunction(hash, dest);
	if(sourceIndex == destIndex)	//lock mutexes
		Hash_lock(hash, sourceIndex);
	else
	{
		Hash_lock(hash, min(sourceIndex, destIndex));
		Hash_lock(hash, max(sourceIndex, destIndex));
	}

	if(Hash_addTransfer(hash, amount, source, dest) < 0)
		sprintf(response_message, "%s%s:%s:%d[:%d])\n", error_message, source, dest, amount, delay);
	else
		sprintf(response_message, "%s%s:%s:%d[:%d])\n", success_message, source, dest, amount, delay);

	//sleep for 'delay' milliseconds
	usleep(delay * 1000);
	if(sourceIndex != destIndex)	//unlock mutexes
	{
		Hash_unlock(hash, sourceIndex);
		Hash_unlock(hash, destIndex);
	}
	else
		Hash_unlock(hash, sourceIndex);

	sendToClient(socket, response_message);
	free(response_message);
	return 0;
}

int addMultiTransfer(int socket, Hash hash, list accounts_list, int amount, int delay)
{
	int *index;
	char *error_message = "Error. Multi-Transfer addition failed (";
	char *success_message = "Success. Multi-Transfer addition (";
	char *name, *response_message;
	list index_list;
	iterator it;
	List_create(&index_list);	//create an ordered list to store the index of each name(doesn't store duplicates)
	List_initIterator(accounts_list, &it);	//accounts' names list
	while(it != NULL)
	{
		if((index = malloc(sizeof(int))) == NULL)
		{
			List_destroy(&index_list);
			return ALLOCATE_ERROR;
		}
		name = List_returnValue(&it);
		*index = Hash_hashFunction(hash, name);
		if(List_add(index_list, index, int_compare) < 0)	//index already exists
			free(index);
	}

	//lock mutexes
	List_initIterator(index_list, &it);
	while(it != NULL)
	{
		index = List_returnValue(&it);
		Hash_lock(hash, *index);
	}

	//sleep for 'delay' milliseconds
	usleep(delay * 1000);

	int error = 0;
	List_initIterator(accounts_list, &it);
	char *src_name = List_returnValue(&it);	//first name in the list is the name of the source account
	int src_balance = Hash_returnBalance(hash, src_name);
	int dst_count = List_returnSize(accounts_list) - 1;	//number of accounts minus 1 (source account)
	if((src_balance < (dst_count * amount)) || checkAccounts(hash, accounts_list) < 0)	//transfer cannot be completed return error
		error = -1;
	else
	{
		while(it != NULL)
		{
			name = List_returnValue(&it);
			if((error = Hash_addTransfer(hash, amount, src_name, name)) < 0)
				break;
		}
	}

	//unlock mutexes
	List_initIterator(index_list, &it);
	while(it != NULL)
	{
		index = List_returnValue(&it);
		Hash_unlock(hash, *index);
	}
	List_destroy(&index_list);

	response_message = malloc(sizeof(char) * (strlen(error_message) + strlen(src_name) + 2 * sizeof(int) + 15));
	if(response_message == NULL)
		return ALLOCATE_ERROR;
	if(error < 0)
		sprintf(response_message, "%s%s:%d [:%d])\n", error_message, src_name, amount, delay);
	else
		sprintf(response_message, "%s%s:%d [:%d])\n", success_message, src_name, amount, delay);

	sendToClient(socket, response_message);
	free(response_message);
	return 0;
}

int printMultiBalance(int socket, Hash hash, list accounts_list)
{
	int *index, message_size = 20;
	char *name;
	list index_list;
	List_create(&index_list);
	iterator it;
	List_initIterator(accounts_list, &it);
	//create an ordered list to store the index of each name(doesn't store duplicates)
	while(it != NULL)
	{
		if((index = malloc(sizeof(int))) == NULL)
		{
			List_destroy(&index_list);
			return ALLOCATE_ERROR;
		}
		name = List_returnValue(&it);
		message_size += strlen(name) + sizeof(int) + 6;
		*index = Hash_hashFunction(hash, name);
		if(List_add(index_list, index, int_compare) < 0)	//index exists
			free(index);
	}

	//lock mutexes
	List_initIterator(index_list, &it);
	while(it != NULL)
	{
		index = List_returnValue(&it);
		Hash_lock(hash, *index);
	}

	char *response_message;
	char *success_message = "Success. Multi-Balance (";
	char *error_message = "Error. Multi-Balance (";
	response_message = malloc(sizeof(char) * (strlen(success_message) + message_size + 4));
	sprintf(response_message, "%s", success_message);
	char balance_string[10];
	int balance;
	List_initIterator(accounts_list, &it);
	while(it != NULL)
	{
		name = List_returnValue(&it);
		balance = Hash_returnBalance(hash, name);
		if(balance < 0)	//account not found ERROR
			break;
		strcat(response_message, name);
		strcat(response_message, "/");
		sprintf(balance_string, "%d", balance);
		strcat(response_message, balance_string);
		strcat(response_message, ":");
	}
	strcat(response_message, ")\n");

	//unlock mutexes
	List_initIterator(index_list, &it);
	while(it != NULL)
	{
		index = List_returnValue(&it);
		Hash_unlock(hash, *index);
	}

	if(balance < 0) //ERROR
	{
		sprintf(response_message, "%s", error_message);
		List_initIterator(accounts_list, &it);
		while(it != NULL)
		{
			strcat(response_message, (char*)List_returnValue(&it));
			strcat(response_message, ":");
		}
		strcat(response_message, ")\n");
	}
	sendToClient(socket, response_message);
	List_destroy(&index_list);
	free(response_message);
	return 0;
}

int printBalance(int socket, Hash hash, char *name)
{
	char *error_message =  "Error. Balance (";
	char *success_message = "Success. Balance (";
	char *response_message = malloc(sizeof(char) * (strlen(success_message) + strlen(name) + sizeof(int) + 15));
	if(response_message == NULL)
		return ALLOCATE_ERROR;
	int index = Hash_hashFunction(hash, name);

	Hash_lock(hash, index);	//lock mutex
	Accountptr acc = Hash_getAccount(hash, name);
	if(acc == NULL)
		sprintf(response_message, "%s%s)\n", error_message, name);
	else
		sprintf(response_message, "%s%s:%d)\n", success_message, name, Account_returnBalance(acc));
	Hash_unlock(hash, index);	//unlock mutex

	sendToClient(socket, response_message);
	free(response_message);
	return 0;
}


int int_compare(void *integer1, void *integer2)
{
	if(*(int*)integer1 < *(int*)integer2)
		return -1;
	else if(*(int*)integer1 == *(int*)integer2)
		return 0;
	else
		return 1;
}

int min(int i, int j)
{
	if(i < j)
		return i;
	else
		return j;
}

int max(int i, int j)
{
	if(i > j)
		return i;
	else
		return j;
}
