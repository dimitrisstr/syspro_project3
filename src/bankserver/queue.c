#include "queue.h"

struct queue
{
	int first;
	int last;
	int size;
	int counter;
	int *array;
};

int Queue_create(Queue *queue, int size)
{
	if((*queue = malloc(sizeof(struct queue))) == NULL)
		return -1;
	if(((*queue)->array = malloc(sizeof(int) * size)) == NULL)
		return -1;
	(*queue)->first = 0;
	(*queue)->last = 0;
	(*queue)->counter = 0;
	(*queue)->size = size;
	return 0;
}

int Queue_add(Queue queue, int element)
{
	if(Queue_isFull(queue))
		return -1;
	else
	{
		queue->counter++;
		queue->array[queue->last] = element;
		queue->last = (queue->last + 1) % queue->size;
		return 0;
	}
}

int Queue_remove(Queue queue)
{
	if(Queue_isEmpty(queue))
		return -1;
	else
	{
		int element;
		queue->counter--;
		element = queue->array[queue->first];
		queue->first = (queue->first + 1) % queue->size;
		return element;
	}
}

int Queue_isEmpty(Queue queue)
{
	return queue->counter == 0;
}

int Queue_isFull(Queue queue)
{
	return queue->counter == queue->size;
}

void Queue_destroy(Queue *queue)
{
	free((*queue)->array);
	free(*queue);

}

