#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include "queue.h"
#include "parse.h"

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#define HASH_SIZE 1000

int sock;
Queue socketQueue;					//queue for the sockets
Hash accounts_hash;
pthread_mutex_t queue_mutex;		//mutex for queue synchronization
pthread_cond_t cond_queue_nonempty, cond_queue_nonfull;

void* master_thread_function();

void* worker_thread_function();

void perror_exit(char *message)
{
	perror(message);
	exit(EXIT_FAILURE);
}

void thread_perror_exit(char *message, int error)
{
	fprintf(stderr, "%s", message);
	strerror(error);
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	/*
	 * arguments
	 */

	if(argc < 7)
	{
		printf("./bankserver -p <port> -s <thread pool size> -q <queue size>\n");
		exit(EXIT_FAILURE);
	}

	int port, threadPoolSize, queueSize;
	port = threadPoolSize = queueSize = 0;
	//read arguments
	while(--argc)
	{
		if(strcmp(argv[argc], "-p") == 0)
			port = atoi(argv[argc + 1]);
		else if(strcmp(argv[argc], "-s") == 0)
			threadPoolSize = atoi(argv[argc + 1]);	//number of worker threads
		else if(strcmp(argv[argc], "-q") == 0)
			queueSize = atoi(argv[argc + 1]);		//queue's size
	}

	if(port == 0 || threadPoolSize == 0 || queueSize == 0)	//invalid arguments
	{
		printf("Wrong arguments.\n");
		exit(EXIT_FAILURE);
	}

	signal(SIGPIPE, SIG_IGN);	//ignore SIGPIPE signal, in case client exits using CTRL + C
	/*
	 * create socket
	 */
	struct sockaddr_in server;
	struct sockaddr *serverptr = (struct sockaddr *) &server;

	if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0)
		perror_exit("socket");

	server.sin_family = AF_INET;		//internet domain
	server.sin_addr.s_addr = htonl(INADDR_ANY);
	server.sin_port = htons(port);

	//bind socket to address
	if(bind(sock, serverptr, sizeof(server)) < 0)
		perror_exit("bind");

	if(listen(sock, SOMAXCONN) < 0)
		perror_exit("listen");

	pthread_mutex_init(&queue_mutex, NULL);
	pthread_cond_init(&cond_queue_nonempty, NULL);
	pthread_cond_init(&cond_queue_nonfull, NULL);

	//create a new queue for the sockets
	if((Queue_create(&socketQueue, queueSize)) < 0)
		perror_exit("queue");
	if(Hash_create(&accounts_hash, HASH_SIZE) < 0)
		perror("hash");

	/*
	 * create threads
	 */
	pthread_t *worker_threads, master_thread;
	int i, err;
	//create master thread
	if((err = pthread_create(&master_thread, NULL, master_thread_function, NULL)))
		thread_perror_exit("master thread:pthread_create", err);
	//create worker threads
	if((worker_threads = malloc(sizeof(pthread_t) * threadPoolSize)) == NULL)
		perror_exit("malloc");
	for(i = 0; i < threadPoolSize; i++)
		if((err = pthread_create(&worker_threads[i], NULL, worker_thread_function, NULL)))
			thread_perror_exit("pthread_create", err);

	//wait for thread termination
	pthread_join(master_thread, NULL);
	for(i = 0; i < threadPoolSize; i++)
		if((err = pthread_join(worker_threads[i], NULL)))
			thread_perror_exit("pthread_join", err);
}

void* master_thread_function()
{
	int newSock;
	while(1)
	{
		newSock = accept(sock, NULL, NULL);
		printf("New connection!\n");
		pthread_mutex_lock(&queue_mutex);	//critical section

		while(Queue_isFull(socketQueue))
			pthread_cond_wait(&cond_queue_nonfull, &queue_mutex);
		Queue_add(socketQueue, newSock);
		pthread_cond_signal(&cond_queue_nonempty);

		pthread_mutex_unlock(&queue_mutex);
	}
}

void* worker_thread_function()
{
	int newSock;
	while(1)
	{
		pthread_mutex_lock(&queue_mutex);

		while(Queue_isEmpty(socketQueue))
			pthread_cond_wait(&cond_queue_nonempty, &queue_mutex);
		//remove a socket from the queue
		newSock = Queue_remove(socketQueue);
		pthread_cond_signal(&cond_queue_nonfull);

		pthread_mutex_unlock(&queue_mutex);
		readCommands(newSock, accounts_hash);
	}
}

