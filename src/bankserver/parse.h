#ifndef PARSE_H_
#define PARSE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "functions.h"

#define EXIT -1
#define PARSE_ERROR -2

void readCommands(int socket, Hash accounts_hash);

int executeCommand(char *command, int socket, Hash accounts_hash);

int isNumber(char *token);

#endif /* PARSE_H_ */
