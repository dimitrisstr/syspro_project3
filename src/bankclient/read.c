#include "read.h"

void readFromFile(char *inputFile, int socket)
{
	FILE *file;
	if((file = fopen(inputFile, "r")) == NULL)
		perror_exit("open");

	char buffer[FILE_BUFFER_SIZE];
	while(fgets(buffer, FILE_BUFFER_SIZE, file) != NULL)
		executeCommand(socket, buffer);
	fclose(file);
}

void readFromUser(int socket)
{
	char buffer[FILE_BUFFER_SIZE];
	printf("> ");
	while(fgets(buffer, FILE_BUFFER_SIZE, stdin) != NULL)
	{
		executeCommand(socket, buffer);
		printf("> ");
	}
}

void sendCommandToServer(int socket, char *command)
{
	uint32_t commandSize;
	int commandLength, sent = 0, n;
	commandLength = strlen(command) + 1;
	commandSize = htonl(commandLength);	//convert to network byte order
	if(write(socket, &commandSize, sizeof(uint32_t)) < 0)	//send size of command
		server_perror_exit();

	while(sent < commandLength)
	{
		if((n = write(socket, command + sent, commandLength - sent)) < 0)	//send command
			server_perror_exit();
		sent += n;
	}
}

void readResponseFromServer(int socket)
{
	char *message;
	uint32_t size;
	if(read(socket, &size, sizeof(uint32_t)) <= 0)
		server_perror_exit();

	size = ntohl(size);
	if((message = malloc(sizeof(char) * size)) == NULL)
	{
		fprintf(stderr, "Error\n");
		return;
	}
	int nread, total = 0;
	while(total < size)
	{
		if((nread = read(socket, message + total, size - total)) <= 0)
			server_perror_exit();
		total += size;
	}
	printf("%s", message);
	free(message);
}

void executeCommand(int socket, char *command)
{
	char command_buffer[FILE_BUFFER_SIZE];
	strcpy(command_buffer, command);
	char *token = NULL;
	if((token = strtok(command_buffer, " \n")) == NULL)
		return;

	if(strcmp(token, "sleep") == 0)	//sleep for "delay" milliseconds
	{
		int delay = 0;
		if((token = strtok(NULL, " \n")) != NULL)
			delay = atoi(token);

		usleep(delay * 1000);
	}
	else if(strcmp(token, "print_commands") == 0)
		printCommands();
	else if(strcmp(token, "exit") == 0)
	{
		shutdown(socket, SHUT_RD);
		exit(EXIT_SUCCESS);
	}
	else
	{
		sendCommandToServer(socket, command);	//send command
		readResponseFromServer(socket);			//receive response
	}
}

void printCommands()
{
	printf("Available commands:\n"
			"add account <init amount> <name> [delay]\n"
			"add transfer <amount> <src name> <dst name> [delay]\n"
			"add multi transfer <amount> <src name> <dst name1> <dst name2> ... [delay]\n"
			"print balance <name>\n"
			"print multi balance <name1> <name2> ...\n"
			"sleep <time>\n"
			"exit\n");
}

void perror_exit(char *message)
{
	perror(message);
	exit(EXIT_FAILURE);
}

void server_perror_exit()
{
	fprintf(stderr, "Connection to server terminated!\n");
	exit(EXIT_FAILURE);
}


