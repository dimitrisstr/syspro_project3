#ifndef READ_H_
#define READ_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1

#define FILE_BUFFER_SIZE 512

void perror_exit(char *message);

void server_perror_exit();

void readFromFile(char *inputFile, int socket);

void readFromUser(int socket);

void sendCommandToServer(int socket, char *command);

void readResponseFromServer(int socket);

void executeCommand(int socket, char *command);

void printCommands();

int isNumber(char *token);

#endif /* READ_H_ */
