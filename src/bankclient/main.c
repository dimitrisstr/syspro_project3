#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>	//sockets
#include <sys/socket.h>	//sockets
#include <netinet/in.h>	//internet sockets
#include <unistd.h>		//read, write, close
#include <netdb.h>		//gethostbyaddr
#include <signal.h>
#include "read.h"



int main(int argc, char *argv[])
{
	if(argc < 7)
	{
		printf("Wrong number of arguments!\n");
		printf("./bankclient -h <server host> -p <server port> -i <command file>\n");
		exit(EXIT_FAILURE);
	}

	signal(SIGPIPE, SIG_IGN);
	char *command_file = NULL, *server_host = NULL;
	int server_port, sock;
	server_port = 0;

	struct sockaddr_in server;
	struct sockaddr *serverptr = (struct sockaddr *) &server;
	struct hostent *sh;

	while(--argc)
	{
		if(strcmp(argv[argc], "-h") == 0)
		{
			server_host = malloc(sizeof(char) * (strlen(argv[argc + 1]) + 1));
			strcpy(server_host, argv[argc + 1]);
		}
		else if(strcmp(argv[argc], "-p") == 0)
			server_port = atoi(argv[argc + 1]);
		else if(strcmp(argv[argc], "-i") == 0)
		{
			command_file = malloc(sizeof(char) * (strlen(argv[argc + 1]) + 1));
			strcpy(command_file, argv[argc + 1]);
		}
	}

	if(server_port == 0 || server_host == NULL || command_file == NULL)
	{
		printf("Wrong arguments!\n");
		printf("./bankclient -h <server host> -p <server port> -i <command file>\n");
		exit(EXIT_FAILURE);
	}

	//create socket
	if ((sock = socket(PF_INET, SOCK_STREAM, 0) ) < 0)
		perror_exit("socket");

	//find server's address
	if((sh = gethostbyname(server_host)) == NULL)
	{
		herror("gethostbyname");
		exit(EXIT_FAILURE);
	}

	server.sin_family = AF_INET;	//address family
	memcpy(&server.sin_addr, sh->h_addr, sh->h_length);
	server.sin_port = htons(server_port);	//server port

	//initiate connection
	if(connect(sock, serverptr, sizeof(server)) < 0)
		perror_exit("connect");

	readFromFile(command_file, sock);
	readFromUser(sock);

	free(command_file);
	free(server_host);
	close(sock);
	exit(EXIT_SUCCESS);
}

